import javax.swing.*;
import java.awt.*;

/**
 * Created by spirok on 13/05/15.
 */
public class PanelColor extends JPanel {

    private Color color;

    public PanelColor(Color c) {
            setColor(c);
    }

    public void setColor(Color c) {
        this.color = c;
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(color);
        // dibujo con el color seteado
        g2d.fillOval(0, 0, 22, 22);
    }
}
