import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Set;

/**
 * Created by Spirok on 16/04/2015.
 */
public class Controlador implements ActionListener {

    private Analizador modelo;
    private Gui2 vista;

    public Controlador(Analizador modelo, JFrame vista) {
        this.modelo = modelo;

        this.vista  = (Gui2)vista;

        this.vista.getBtnSelectArchivo().addActionListener(this);
        this.vista.getBtnProcesar().addActionListener(this);

        this.vista.setearSemaforo();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.getBtnSelectArchivo()) { // click boton seleccionar archivo
            int returnVal;
            String rutaArchivo;
            JFileChooser chooser  = new JFileChooser();
            returnVal = chooser.showOpenDialog(vista.getContentPane());
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                rutaArchivo = chooser.getSelectedFile().getAbsolutePath();
                if (!modelo.setArchivo( new File(rutaArchivo) )) {
                    System.err.println("Error!");
                    JOptionPane.showMessageDialog(null, "Error!");
                    return;
                }
                System.out.println("archivo elejido : " + rutaArchivo);
                System.out.println("lenguaje : " + modelo.getLenguajeActual());
                vista.getTxtRutaArchivo().setText(rutaArchivo);
                vista.getBtnProcesar().setEnabled(true);
            }
        }
        if (e.getSource() == vista.getBtnProcesar()) { // click boton procesar
            modelo.procesarAnalisis();

            System.out.println("Cantidad de lineas vacias : " + modelo.getCantidadLineasVacio());
            System.out.println("Cantidad de lineas con comentario  : " + modelo.getCantidadLineasComentario());
            System.out.println("Cantidad de lineas con codigo : " + modelo.getCantidadLineasCodigo());
            System.out.println("Cantidad de lineas totales : " + modelo.getCantidadLineas());

            // muestro codigo en jtextarea
            vista.getJtCodigo().setText(modelo.getBufferCodigo().toString());

            vista.getLblInfoLineas().setText("<html> <font size='5'><p align='center'> INFORMACION : </p> </font>" +
                    "Lenguaje : " + modelo.getLenguajeActual() + "<br>" +
                    "Cantidad de lineas vacias : " + modelo.getCantidadLineasVacio() + "<br>" +
                    "Cantidad de lineas codigo : " + modelo.getCantidadLineasCodigo() + "<br>" +
                    "Cantidad de lineas comentarios : " + modelo.getCantidadLineasComentario() + "<br>" +
                    "Cantidad total : " + modelo.getCantidadLineas() + "<br>" +
                    "Porcentaje de comentarios : " + modelo.getPorcentajeComentario() + "% <br>" +
                    "<font size='4'>Calificacion Final : </font> <br>" +
                    modelo.getEvaluacion() + "<br>" +
                    "</html>");

            this.vista.setearSemaforo();

            // Menos del 10% se lo considera muy poco comentado, mas de 30% comentado excesivamente.
            if ((modelo.getPorcentajeComentario() < 10) || modelo.getPorcentajeComentario() > 30) {
                vista.getPanelRojo().setColor(Color.RED);
                vista.getPanelRojo().repaint();
            }
            // Entre el 10% a 20% se lo considerara normal.
            if ( (modelo.getPorcentajeComentario() >= 10.0) && (modelo.getPorcentajeComentario() <= 20.0 )) {
                vista.getPanelAmarillo().setColor(Color.YELLOW);
                vista.getPanelAmarillo().repaint();
            }
            // Entre el 20% a 30% bien comentado (optimo).
            if ( (modelo.getPorcentajeComentario() > 20.0) && (modelo.getPorcentajeComentario() <= 30.0 ) ) {
                vista.getPanelVerde().setColor(Color.GREEN);
                vista.getPanelVerde().repaint();
            }
            vista.repaint();
        }
    }
}
