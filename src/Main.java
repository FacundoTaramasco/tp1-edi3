import javax.swing.*;

/**
 * Created by Spirok on 15/04/2015.
 */
public class Main {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            //UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch(Exception e) {
            System.out.printf(e.getMessage());
        }
        Analizador modelo = new Analizador();
        JFrame vista = new Gui2();
        new Controlador(modelo, vista);

    }
}
