import javax.swing.*;
import java.awt.*;

/**
 * Created by Spirok on 15/04/2015.
 */
public class Gui2 extends JFrame {

    private JTextField txtRutaArchivo;
    private JButton btnSelectArchivo;
    private JButton btnProcesar;

    private JLabel lblInfoLineas;

    private JTextArea jtCodigo;

    private GridBagConstraints gbc = new GridBagConstraints();

    private PanelColor panelVerde;
    private PanelColor panelAmarillo;
    private PanelColor panelRojo;

    public PanelColor getPanelVerde() {
        return panelVerde;
    }
    public PanelColor getPanelAmarillo() {
        return panelAmarillo;
    }
    public PanelColor getPanelRojo() {
        return panelRojo;
    }

    //private JPanel panelNorte;

    public Gui2() {
        super("TP1-EDI3. GUI ALPHA");
        this.getContentPane().setLayout( new GridBagLayout());

        txtRutaArchivo = new JTextField();
        txtRutaArchivo.setEditable(false);

        btnSelectArchivo = new JButton("Seleccionar...");
        btnProcesar = new JButton("Procesar");

        lblInfoLineas = new JLabel("<html> <font size='5'><p align='center'> INFORMACION : </p> </font>" +
                "Lenguaje :  <br> " +
                "Cantidad de lineas vacias : 0<br>" +
                "Cantidad de lineas codigo : 0<br>" +
                "Cantidad de lineas comentarios : 0<br>" +
                "Cantidad total : 0 <br> " +
                "Porcentaje de comentarios : 0% <br>" +
                "<font size='4'>Calificacion Final : </font> <br> <br>" +
                "</html>");

        Icon info = new ImageIcon(getClass().getResource("info.png"));
        JLabel lbl = new JLabel();
        lbl.setIcon(info);

        lbl.setToolTipText("<html>Lenguajes soportados : <br>" + Analizador.getLenguajes() + " </html>");

        agregarWidget(new JLabel("Ruta Archivo : "), 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE);
        agregarWidget(txtRutaArchivo,                1, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL);
        agregarWidget(btnSelectArchivo,              2, 0, 1, 1, 0, 0, GridBagConstraints.HORIZONTAL);
        agregarWidget(lbl,                           3, 0, 1, 1, 0, 0, GridBagConstraints.NONE);

        agregarWidget(new JLabel(""),                0, 1, 2, 1, 1, 0, GridBagConstraints.HORIZONTAL);
        agregarWidget(btnProcesar,                   2, 1, 1, 1, 0, 0, GridBagConstraints.NONE);

        agregarWidget(lblInfoLineas,                 1, 2, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL);

        JPanel panelSemaforo = new JPanel(new GridLayout(3, 1, 5, 10));

        panelRojo     = new PanelColor(Color.RED);
        panelAmarillo = new PanelColor(Color.YELLOW);
        panelVerde    = new PanelColor(Color.GREEN);


        panelVerde.add(new JLabel("Verde"));
        panelAmarillo.add(new JLabel("Amarillo"));
        panelRojo.add(new JLabel("Rojo"));

        panelSemaforo.add(panelRojo);
        panelSemaforo.add(panelAmarillo);
        panelSemaforo.add(panelVerde);

        agregarWidget( panelSemaforo, 2, 2, 2, 1, 0, 0, GridBagConstraints.NONE);

        jtCodigo = new JTextArea();
        jtCodigo.setEditable(false);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(jtCodigo);

        agregarWidget(scrollPane, 0, 3, 4, 1, 1, 1, GridBagConstraints.BOTH);

        btnProcesar.setEnabled(false);

        this.setSize(800, 600);
        this.setVisible(true);
        //this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public void agregarWidget(JComponent widget, int gridx, int gridy, int gridwidth,
                              int gridheight , int weightx, int weighty, int fill ) {
        gbc.gridx      = gridx;      // en que columna
        gbc.gridy      = gridy;      // en que fila
        gbc.gridwidth  = gridwidth;  // ancho de celda
        gbc.gridheight = gridheight; // alto de celda
        gbc.weightx    = weightx;    // cuanto crece en ancho al redimensionarse
        gbc.weighty    = weighty;    // cuanto crece en alto al redimensionarse
        gbc.fill       = fill;       // comportamiento al crecer
        this.add(widget, gbc); // agrega widget al JIF con las restricciones establecidas
    }

    public void setearSemaforo() {
        panelVerde.setColor(Color.DARK_GRAY);
        panelVerde.repaint();
        panelAmarillo.setColor(Color.DARK_GRAY);
        panelAmarillo.repaint();
        panelRojo.setColor(Color.DARK_GRAY);
        panelRojo.repaint();
    }

    public JLabel getLblInfoLineas() {
        return lblInfoLineas;
    }

    public JTextArea getJtCodigo() {
        return jtCodigo;
    }

    public JTextField getTxtRutaArchivo() {
        return txtRutaArchivo;
    }

    public JButton getBtnSelectArchivo() {
        return btnSelectArchivo;
    }

    public JButton getBtnProcesar() {
        return btnProcesar;
    }




}
