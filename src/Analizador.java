import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Spirok on 16/04/2015.
 * Ver archivo LEEME.txt en la raiz del proyecto.
 */
public class Analizador {

    /*
    Ensamblador .ASM
    C   .h .c
    C++ .h .hh .hpp .hxx .h++ .cc .cpp .cxx .c++
    C# .cs
    Java .java
    Delphi .pas
    Sql .sql
    JavaScript .js
    Php .php
    Ruby .rb
    Python .py
    Perl .pl
    Visual Basic .vb
    */

    private enum Lenguajes {ENSAMBLADOR, C, CPLUSPLUS, CSHARP, JAVA, DELPHI, 
                           SQL, JAVASCRIPT, PHP, RUBY, PYTHON, PERL, VISUALBASIC };

    private Lenguajes lenguajeActual;

    private int cantidadLineas;
    private int cantidadLineasVacio;
    private int cantidadLineasCodigo;
    private int cantidadLineasComentario;
    private double porcentajeComentario;
    private String evaluacion;

    private File archivo;

    private final String extensionASM = ".ASM";
    private final ArrayList<String> extensionesC         = new ArrayList<String>(Arrays.asList(".h", ".c"));
    private final ArrayList<String> extensionesCPlusPlus = new ArrayList<String>(Arrays.asList(".h", ".c",
                                                                   ".hh", ".hpp",
                                                                  ".hxx", "h.++", 
                                                                  ".cc", ".cxx", ".c++"));
    private final String extensionCSharp    = ".cs";
    private final String extensionJava      = ".java";
    private final String extensionJDelhpi   = ".pas";
    private final String extensionSql       = ".sql";
    private final String extensionJs        = ".js";
    private final String extensionPhp       = ".php";
    private final String extensionRuby      = ".rb";
    private final String extensionPython    = ".py";
    private final String extensionPerl      = ".pl";
    private final String extensionVb        = ".vb";

    private StringBuffer bufferCodigo = new StringBuffer();

    private String simboloCmtLinea;
    private String simboloCmtIBloque;
    private String simboloCmtFBloque;

    /**
     * Constructor
     */
    public Analizador() {
        this(null);
    }

    /**
     * Constructor
     * @param archivo utilizado para ser analizado.
     */
    public Analizador(File archivo) {
        setArchivo(archivo);
        setCantidadLineas(0);
        setCantidadLineasVacio(0);
        setCantidadLineasCodigo(0);
        setCantidadLineasComentario(0);
    }

    // Getters
    public int getCantidadLineas() { return cantidadLineas; }

    public int getCantidadLineasVacio() { return cantidadLineasVacio; }

    public int getCantidadLineasCodigo() { return cantidadLineasCodigo; }

    public int getCantidadLineasComentario() { return cantidadLineasComentario; }

    public Lenguajes getLenguajeActual() { return lenguajeActual; }

    public File getArchivo() { return archivo; }

    public StringBuffer getBufferCodigo() { return bufferCodigo; }

    public double getPorcentajeComentario() { return porcentajeComentario; }

    public String getEvaluacion() { return evaluacion; }

    private String getSimboloCmtLinea() { return simboloCmtLinea; }

    private String getSimboloCmtIBloque() { return simboloCmtIBloque; }

    private String getSimboloCmtFBloque() { return simboloCmtFBloque; }

    // Setters
    private void setCantidadLineas(int cantidadLineas) { this.cantidadLineas = cantidadLineas; }

    private void setCantidadLineasVacio(int cantidadLineasVacio) { this.cantidadLineasVacio = cantidadLineasVacio; }

    private void setCantidadLineasCodigo(int cantidadLineasCodigo) { this.cantidadLineasCodigo = cantidadLineasCodigo; }

    private void setCantidadLineasComentario(int cantidadLineasComentario) { this.cantidadLineasComentario = cantidadLineasComentario; }

    public boolean setArchivo(File archivo) {
        if (archivo == null)
            return false;
        if( (!archivo.exists()) ||(!archivo.canRead()) || (!archivo.isFile()) )
            return false;
        if (!chequearExtension(archivo))
            return false;
        this.archivo = archivo;
        return true;
    }

    private void setLenguajeActual(Lenguajes lenguajeActual) { this.lenguajeActual = lenguajeActual; }

    private void setPorcentajeComentario(double porcentaje) { this.porcentajeComentario = porcentaje; }

    private void setEvaluacion(String evaluacion) { this.evaluacion = evaluacion; }

    private void setSimboloCmtLinea(String simbolo) { simboloCmtLinea = simbolo; }

    private void setSimboloCmtIBloque(String simbolo) { simboloCmtIBloque = simbolo; }

    private void setSimboloCmtFBloque(String simbolo) { simboloCmtFBloque = simbolo; }

    // Customs

    /**
     * Metodo que retorna en formato String todos los lenguajes
     * con el cual trabaja la app.
     * @return String cadena con los lenguajes utilzados.
     */
    public static String getLenguajes() {
        StringBuffer buffer = new StringBuffer("");
        for (Lenguajes len : Lenguajes.values()) {
            buffer.append(len.toString() + " ");
        }
        return buffer.toString();
    }

    /**
     * Metodo que chequea la extension del archivo a analizar
     * @param archivo que se utilizara para analizar
     * @return boolean true si es una extension valida, false contrario.
     */
    private boolean chequearExtension(File archivo) {
        String rutaArchivo = archivo.getAbsolutePath();
        // chequeando la extension del archivo

        if (rutaArchivo.lastIndexOf(".") == -1) return false;
        String ext = rutaArchivo.substring(rutaArchivo.lastIndexOf("."), rutaArchivo.length());
        if (extensionASM.equals(ext))
            setLenguajeActual(Lenguajes.ENSAMBLADOR);
        else if (extensionesC.contains(ext))
            setLenguajeActual(Lenguajes.C);
        else if (extensionesCPlusPlus.contains(ext))
            setLenguajeActual(Lenguajes.CPLUSPLUS);
        else if (extensionCSharp.equals(ext))
            setLenguajeActual(Lenguajes.CSHARP);
        else if (extensionJava.equals(ext))
            setLenguajeActual(Lenguajes.JAVA);
        else if (extensionJDelhpi.equals(ext))
            setLenguajeActual(Lenguajes.DELPHI);
        else if (extensionSql.equals(ext))
            setLenguajeActual(Lenguajes.SQL);
        else if (extensionJs.equals(ext))
            setLenguajeActual(Lenguajes.JAVASCRIPT);
        else if (extensionPhp.equals(ext))
            setLenguajeActual(Lenguajes.PHP);
        else if (extensionRuby.equals(ext))
            setLenguajeActual(Lenguajes.RUBY);
        else if (extensionPython.equals(ext))
            setLenguajeActual(Lenguajes.PYTHON);
        else if (extensionPerl.equals(ext))
            setLenguajeActual(Lenguajes.PERL);
        else if (extensionVb.equals(ext))
            setLenguajeActual(Lenguajes.VISUALBASIC);
        else
            return false;

        if ( (getLenguajeActual() == Lenguajes.C) || (getLenguajeActual() == Lenguajes.CPLUSPLUS) ||
                (getLenguajeActual() == Lenguajes.CSHARP) || (getLenguajeActual() == Lenguajes.JAVA) ||
                (getLenguajeActual() == Lenguajes.JAVASCRIPT)) {
            setSimboloCmtLinea("//");
            setSimboloCmtIBloque("/*");
            setSimboloCmtFBloque("*/");
        }
        if (getLenguajeActual() == Lenguajes.VISUALBASIC) {
            setSimboloCmtLinea("'");
            setSimboloCmtIBloque(null);
            setSimboloCmtFBloque(null);
        }
        if (getLenguajeActual() == Lenguajes.DELPHI) {
            setSimboloCmtLinea("//");
            setSimboloCmtIBloque("(*");
            setSimboloCmtFBloque("*)");
        }
        return true;
    }

    /**
     * Metodo que determina la evaluacion final con respecto a la cantidad
     * de lineas de comentarios en el codigo fuente.
     */
    private void realizarEvaluacion() {
        // calculando porcentaje de lineas con comentario
        // cantidad total lineas     --> 100 %
        // cantidad de comentarios   -->  =   cantidadcomentarios*100 / cantidad total lineas
        setPorcentajeComentario((getCantidadLineasComentario() * 100) / getCantidadLineas());
        System.out.println("porcentaje de comentarios : " + getPorcentajeComentario() + "%");

        // Menos del 10% se lo considera muy poco comentado.
        if (getPorcentajeComentario() < 10.0) {
            setEvaluacion("El porcentaje de comentarios en el codigo fuente es menor al 10% (poco)");
        } // Entre el 10% a 20% se lo considerara normal.
        else if ( (getPorcentajeComentario() >= 10.0) && (getPorcentajeComentario() <= 20.0 ) ) {
            setEvaluacion("El porcentaje de comentarios en el codigo fuente se encuentra entre el 10-20% (bien)");
        } // Entre el 20% a 30% bien comentado (optimo).
        else if ( (getPorcentajeComentario() > 20.0) && (getPorcentajeComentario() <= 30.0 ) ) {
            setEvaluacion("El porcentaje de comentarios en el codigo fuente se encuentra entre el 20-30% (optimo)");
        }// Mas de 30% comentado excesivamente.
        else if (getPorcentajeComentario() > 30.0) {
            setEvaluacion("El porcentaje de comentarios en el codigo fuente es mayor al 30% (excesivo)");
        } else
            setEvaluacion(null);
    }

    /**
     * Metodo que apartir del archivo elejido analiza su contenido
     * (codigo, comentarios, espacios en blanco) y los contabiliza.
     */
    public void procesarAnalisis() {
        if (archivo == null) return;
        if (getLenguajeActual() == null) return;
        setCantidadLineas(0);
        setCantidadLineasVacio(0);
        setCantidadLineasCodigo(0);
        setCantidadLineasComentario(0);
        setPorcentajeComentario(0);
        if ( (getLenguajeActual() == Lenguajes.C) || (getLenguajeActual() == Lenguajes.CPLUSPLUS) ||
                (getLenguajeActual() == Lenguajes.CSHARP) || (getLenguajeActual() == Lenguajes.JAVA) ||
                (getLenguajeActual() == Lenguajes.JAVASCRIPT)) {
            analisisCmtLineaBloqueGenerico();
        }
        if (getLenguajeActual() == Lenguajes.VISUALBASIC) {
            analisisCmtLineaGenerico();
        }
        if (getLenguajeActual() == Lenguajes.DELPHI) {
            analisisCmtLineaBloqueDelphi();
        }
    }



    private void analisisCmtLineaGenerico() {
        String lineaActual;
        int posCmtLinea;
        int codigoYcomentario = 0;
        BufferedReader br;
        bufferCodigo.delete(0, bufferCodigo.length());
        try {
            br = new BufferedReader(new FileReader( getArchivo() ));
            while ((lineaActual = br.readLine()) != null) { // leo mientras no sea fin de archivo
                System.out.println(lineaActual);
                bufferCodigo.append(lineaActual + "\n");
                lineaActual = lineaActual.trim(); // le quito espacios de izq. y der.

                posCmtLinea = lineaActual.indexOf( getSimboloCmtLinea() );

                if (lineaActual.equals("")) {  // linea vacia
                    setCantidadLineasVacio(getCantidadLineasVacio() + 1);
                    continue;
                }

                if (posCmtLinea == -1) { // no hay comentarios
                    setCantidadLineasCodigo(getCantidadLineasCodigo() + 1);
                    continue;
                }

                if (posCmtLinea == 0) { // hay comentario linea en la primer posicion de la linea
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                    continue;
                }

                if (posCmtLinea > 1) { // hay codigo y comentario de linea
                    setCantidadLineasCodigo(getCantidadLineasCodigo() + 1);
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                    codigoYcomentario++;
                    //continue;
                }
            }
            // calculado la cantidad de lineas totales
            setCantidadLineas(getCantidadLineas()  + getCantidadLineasVacio() +
                    getCantidadLineasCodigo() + getCantidadLineasComentario() - codigoYcomentario);

            realizarEvaluacion();

            // In JDK 7, which use try-with-resources new feature to close file automatically.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void analisisCmtLineaBloqueGenerico() {
        String lineaActual;
        int posCmtLinea;
        int posIcmtBloque;
        int posFcmtBloque;
        int codigoYcomentario = 0;
        BufferedReader br;
        bufferCodigo.delete(0, bufferCodigo.length());
        try {
            br = new BufferedReader(new FileReader( getArchivo() ));
            while ((lineaActual = br.readLine()) != null) { // leo mientras no sea fin de archivo
                System.out.println(lineaActual);
                bufferCodigo.append(lineaActual + "\n");
                lineaActual = lineaActual.trim(); // le quito espacios de izq. y der.

                posCmtLinea   = lineaActual.indexOf( getSimboloCmtLinea() );
                posIcmtBloque = lineaActual.indexOf( getSimboloCmtIBloque() );
                posFcmtBloque = lineaActual.indexOf(getSimboloCmtFBloque());

                if (lineaActual.equals("")) {  // linea vacia
                    setCantidadLineasVacio(getCantidadLineasVacio() + 1);
                    continue;
                }

                if ((posCmtLinea == -1) && (posIcmtBloque == -1)) { // no hay comentarios
                    setCantidadLineasCodigo(getCantidadLineasCodigo() + 1);
                    continue;
                }

                if (posCmtLinea == 0) { // hay comentario linea en la primer posicion de la linea
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                    continue;
                }

                // hay comentario de bloque de varias lineas, no las tomo en cuenta
                if (posIcmtBloque >= 0 && posFcmtBloque == -1) {
                    // lo tomo como solo una linea de comentarios al bloque??
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                    //setCantidadLineas(getCantidadLineas() + 1);
                    while ((lineaActual = br.readLine()) != null) {
                        System.out.println(lineaActual);
                        bufferCodigo.append(lineaActual + "\n");
                        lineaActual = lineaActual.trim();
                        posFcmtBloque = lineaActual.indexOf(getSimboloCmtFBloque());
                        if (posFcmtBloque == -1)
                            setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                            //setCantidadLineas(getCantidadLineas() + 1);
                        else
                            break;
                    }
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                    //setCantidadLineas(getCantidadLineas() + 1);
                    continue;
                }

                if (posCmtLinea > 1) { // hay codigo y comentario de linea
                    setCantidadLineasCodigo(getCantidadLineasCodigo() + 1);
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                    codigoYcomentario++;
                    continue;
                }

                if ( (posIcmtBloque >= 0) && (posFcmtBloque > 0) ) { // hay comentario de bloque en la linea
                    // tener en cuenta que puede haber codigo tambien...
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                }
            }
            // calculado la cantidad de lineas totales
            setCantidadLineas(getCantidadLineas()  + getCantidadLineasVacio() +
                    getCantidadLineasCodigo() + getCantidadLineasComentario() - codigoYcomentario);

            realizarEvaluacion();

            // In JDK 7, which use try-with-resources new feature to close file automatically.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void analisisCmtLineaBloqueDelphi() {
        String lineaActual;
        int posCmtLinea;
        int posIcmtBloque;
        int posFcmtBloque;
        int posIcmtBloqueAux;
        int posFcmtBloqueAux;
        int codigoYcomentario = 0;
        BufferedReader br;
        bufferCodigo.delete(0, bufferCodigo.length());
        try {
            br = new BufferedReader(new FileReader( getArchivo() ));
            while ((lineaActual = br.readLine()) != null) { // leo mientras no sea fin de archivo
                System.out.println(lineaActual);
                bufferCodigo.append(lineaActual + "\n");
                lineaActual = lineaActual.trim(); // le quito espacios de izq. y der.

                posCmtLinea   = lineaActual.indexOf( getSimboloCmtLinea() );
                posIcmtBloque = lineaActual.indexOf( getSimboloCmtIBloque() );
                posFcmtBloque = lineaActual.indexOf( getSimboloCmtFBloque() );

                posIcmtBloqueAux = lineaActual.indexOf("{");
                posFcmtBloqueAux = lineaActual.indexOf("}");

                if (lineaActual.equals("")) {  // linea vacia
                    setCantidadLineasVacio(getCantidadLineasVacio() + 1);
                    continue;
                }

                if (lineaActual.indexOf("{$") == 0) { // directivas
                    setCantidadLineasCodigo(getCantidadLineasCodigo() + 1);
                    System.out.println("DIRECTIVA ENCONTRADA : " + lineaActual);
                    continue;
                }

                if ((posCmtLinea == -1) && (posIcmtBloque == -1) && (posIcmtBloqueAux == -1 )) { // no hay comentarios
                    setCantidadLineasCodigo(getCantidadLineasCodigo() + 1);
                    continue;
                }

                if (posCmtLinea == 0) { // hay comentario linea en la primer posicion de la linea
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                    continue;
                }

                // hay comentario de bloque de varias lineas, no las tomo en cuenta
                if (posIcmtBloque >= 0 && posFcmtBloque == -1) {
                    // lo tomo como solo una linea de comentarios al bloque??
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                    //setCantidadLineas(getCantidadLineas() + 1);
                    while ((lineaActual = br.readLine()) != null) {
                        System.out.println(lineaActual);
                        bufferCodigo.append(lineaActual + "\n");
                        lineaActual = lineaActual.trim();
                        posFcmtBloque = lineaActual.indexOf(getSimboloCmtFBloque());
                        if (posFcmtBloque == -1)
                            setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                            //setCantidadLineas(getCantidadLineas() + 1);
                        else
                            break;
                    }
                    setCantidadLineas(getCantidadLineas() + 1);
                    continue;
                }

                if (posCmtLinea > 1) { // hay codigo y comentario de linea
                    setCantidadLineasCodigo(getCantidadLineasCodigo() + 1);
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                    codigoYcomentario++;
                    continue;
                }

                if ( (posIcmtBloque >= 0) && (posFcmtBloque > 0) ) { // hay comentario de bloque en la linea
                    // tener en cuenta que puede haber codigo tambien...
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                }

                // EVALUANDO COMENTARIO TIPO { }
                // hay comentario de bloque de varias lineas, no las tomo en cuenta
                if (posIcmtBloqueAux >= 0 && posFcmtBloqueAux == -1) {
                    // lo tomo como solo una linea de comentarios al bloque??
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                    //setCantidadLineas(getCantidadLineas() + 1);
                    while ((lineaActual = br.readLine()) != null) {
                        System.out.println(lineaActual);
                        bufferCodigo.append(lineaActual + "\n");
                        lineaActual = lineaActual.trim();
                        posFcmtBloqueAux = lineaActual.indexOf("}");
                        if (posFcmtBloqueAux == -1)
                            setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                            //setCantidadLineas(getCantidadLineas() + 1);
                        else
                            break;
                    }
                    setCantidadLineas(getCantidadLineas() + 1);
                    continue;
                }


                if ( (posIcmtBloqueAux >= 0) && (posFcmtBloqueAux > 0) ) { // hay comentario de bloque en la linea
                    // tener en cuenta que puede haber codigo tambien...
                    setCantidadLineasComentario(getCantidadLineasComentario() + 1);
                }
            }
            // calculado la cantidad de lineas totales
            setCantidadLineas(getCantidadLineas()  + getCantidadLineasVacio() +
                    getCantidadLineasCodigo() + getCantidadLineasComentario() - codigoYcomentario);

            realizarEvaluacion();

            // In JDK 7, which use try-with-resources new feature to close file automatically.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}