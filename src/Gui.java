import javax.swing.*;
import java.awt.*;

/**
 * Created by Spirok on 15/04/2015.
 */
public class Gui extends JFrame {

    private JTextField txtRutaArchivo;
    private JComboBox  cbLenguaje;
    private JButton btnSelectArchivo;
    private JButton btnProcesar;

    public Gui() {
        super("TP1-EDI3. GUI ALPHA");
        this.getContentPane().setLayout(new BorderLayout(5, 5));

        JPanel jpArriba = new JPanel(new GridLayout(2, 4));

        txtRutaArchivo = new JTextField();

        txtRutaArchivo.setEditable(false);

        cbLenguaje = new JComboBox<String>();

        btnSelectArchivo = new JButton("Seleccionar...");
        btnProcesar = new JButton("Procesar");

        System.out.println(Analizador.getLenguajes());
        Icon info = new ImageIcon(getClass().getResource("info.png"));
        JLabel lbl = new JLabel();
        lbl.setIcon(info);
        //lbl.setToolTipText("<html>Lenguajes soportados : <br>" +
        //        "C++, Java, Delphi, Sql, JavaScript, Php, C, <br>" +
        //        "Ensamblador, Ruby, Python, Perl, Php, Visual Basic </html>");

        lbl.setToolTipText("<html>Lenguajes soportados : <br>" + Analizador.getLenguajes() +" </html>");

        jpArriba.add(new JLabel("Ruta Archivo : "));
        jpArriba.add(txtRutaArchivo);
        jpArriba.add(btnSelectArchivo);
        jpArriba.add(lbl);

        //jpArriba.add(new JLabel("Lenguaje : "));
        //jpArriba.add(cbLenguaje);
        //jpArriba.add(new JLabel(" "));
        //jpArriba.add(new JLabel(" "));

        jpArriba.add(new JLabel(" "));
        jpArriba.add(new JLabel(" "));
        jpArriba.add(btnProcesar);
        jpArriba.add(new JLabel(" "));

        btnProcesar.setEnabled(false);

        this.add(jpArriba, "North");
        this.setSize(480, 300);
        this.setVisible(true);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public JTextField getTxtRutaArchivo() {
        return txtRutaArchivo;
    }

    public JButton getBtnSelectArchivo() {
        return btnSelectArchivo;
    }

    public JButton getBtnProcesar() {
        return btnProcesar;
    }

    public JComboBox getCbLenguaje() {
        return cbLenguaje;
    }

}
