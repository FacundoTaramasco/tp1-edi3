
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

import java.util.*;

public class Servidor extends JFrame {

    /*************************** MAIN *****************************************/
    /**************************************************************************/
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            //UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        new Servidor();
    }

    /**************************************************************************/
    /**************************************************************************/

    private ServerSocket servidor;
    private int          puerto;

    private JTextField txtPuerto;
    private JButton    btnInicar;
    private JButton    btnDetener;
    private JTextArea  txtBuffer;
    private JTextField txtMensaje;

    private HashMap<Integer, Socket> listaSockets         = new HashMap<Integer, Socket>();
    private HashMap<Integer, ObjectOutputStream> listaOUS = new HashMap<Integer, ObjectOutputStream>();

    /*
    Consctructor
    */
    public Servidor() {
        super("Servidor");

        this.getContentPane().setLayout( new BorderLayout(2, 2));

        JPanel panelNorte = new JPanel( new GridLayout(1, 5));
        txtPuerto         = new JTextField();
        txtPuerto.setText("5050");
        btnInicar         = new JButton("Iniciar");
        btnDetener        = new JButton("Detener");

        panelNorte.add(new JLabel("Puerto : "));
        panelNorte.add(txtPuerto);
        panelNorte.add(btnInicar);
        panelNorte.add(btnDetener);
        panelNorte.add(new JLabel(""));

        txtBuffer  = new JTextArea();
        txtBuffer.setEditable(false);
        txtBuffer.setEnabled(false);

        txtMensaje = new JTextField();
        txtMensaje.setEnabled(false);

        this.add( panelNorte, "North");
        this.add(new JScrollPane(txtBuffer) , "Center");
        this.add(txtMensaje, "South");

        this.setSize(490, 500);
        this.setResizable(false);
        this.setVisible(true);
        this.setLocationRelativeTo(null);

        // evento 'enter' widget txtMensaje
        txtMensaje.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new MensajeAclientes(txtMensaje.getText()).start();
            }
        });

        // evento cerrar JFrame
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                cerrarServidor();
                System.exit(0);
            }
        });

        // evento 'click' widget btnInicar
        btnInicar.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                InitServidor server = new InitServidor();
                if (server.setearPuerto()) {
                    server.start();
                    txtBuffer.setEnabled(true);
                    txtMensaje.setEnabled(true);
                    txtPuerto.setEnabled(false);
                } else 
                    txtBuffer.setText(txtPuerto.getText() + " debe ser numero entero");
            }
        });

        // evento 'click' widget btnDetener
        btnDetener.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cerrarServidor();
                txtBuffer.setEnabled(false);
                txtMensaje.setEnabled(false);
                txtPuerto.setEnabled(true);
                txtBuffer.setText("");
            }
        });
    } // fin constructor

    /*
    Clase Thread que inicia un serverSocket en el puerto especificado y espera la conexion
    de algun cliente
    */
    private class InitServidor extends Thread {

        public boolean setearPuerto() {
            try {
                puerto = Integer.parseInt(txtPuerto.getText());
                return true;
            } catch(NumberFormatException nfe) {
                return false;
            } 
        }

        public void run() {
            try {
                servidor = new ServerSocket(puerto);
                txtBuffer.setText("Server Iniciado escuchando puerto : " + puerto);
                while( true ) {
                    Socket sCli = servidor.accept();
                    txtBuffer.append("\nCLIENTE CONECTADO " + sCli.toString() );
                    /* cuando un cliente se conecta al servidor se crea un hilo
                    para poder manejarlo concurrentemente. */
                    listaSockets.put( sCli.getPort(), sCli);
                    listaOUS.put(sCli.getPort(), new ObjectOutputStream(sCli.getOutputStream()));
                    Thread hiloCliente = new hiloCliente(sCli);
                    hiloCliente.start();
                }
            } catch(IOException ioe) {
                System.err.println( ioe.getMessage() );
            } finally {
                cerrarServidor();
            }
        }
    }

    /**
    * Metodo que cierra todos los flujos utilizados con los clientes y el servidor
    */
    private void cerrarServidor() {
        try {
            Set <Integer> claves = listaSockets.keySet();
            for (Integer clave : claves) {
                listaSockets.get(clave).getInputStream().close();
                listaSockets.get(clave).getOutputStream().close();
                listaSockets.get(clave).close();
            }
            if (servidor != null) servidor.close();
        } catch(IOException ioe) {
            System.err.println(ioe.getMessage());
        }
    }


    /*
    Clase interna que le envia un mensaje a todos los clientes al producirse
    evento 'enter' en widget txtMensaje
    */
    private class MensajeAclientes extends Thread {

        private TDAMensaje OMC          = null;

        public MensajeAclientes(String msj) {
            OMC = new TDAMensaje("Servidor", msj );
        }
        public void run() {
            if (servidor == null) return;
            try {
                txtMensaje.setText("");
                // obtengo lista de claves
                Set <Integer> claves = listaOUS.keySet();
                for (Integer clave : claves) {
                    listaOUS.get(clave).writeObject(OMC);
                    listaOUS.get(clave).flush();
                }
                if (!listaOUS.isEmpty())
                    txtBuffer.append("\nServidor a todos los clientes : " + OMC.getMensaje());
            } catch (IOException ioe) {
                txtBuffer.append("\n" + ioe.getMessage());
            }
        }
    }

    /* *************************************************************** */
    /* Clase interna que atiende a un cliente al momento de conectarse */
    /* *************************************************************** */
    private class hiloCliente extends Thread {

        private Socket sCli                    = null;
        private ObjectInputStream flujoEntrada = null;
        private TDAMensaje OMC                 = null;

        // Constructor
        public hiloCliente(Socket sCli) {
            this.sCli = sCli;
        }

        public void run() {
            try {
                // obtengo el flujo del cliente que se conecto
                flujoEntrada = new ObjectInputStream( sCli.getInputStream() );
                while(true) {
                    // leo el objeto enviado por el cliente
                    Object obj = flujoEntrada.readObject();
                    if (obj == null || !(obj instanceof TDAMensaje))
                        continue;
                    OMC = (TDAMensaje)obj; // casteo
                    if (OMC.getMensaje().equalsIgnoreCase("----------QuitClient----------")) {
                        listaSockets.remove(this.sCli.getPort() );
                        listaOUS.remove(this.sCli.getPort() );
                        txtBuffer.append("\nCLIENTE DESCONECTADO");
                        break;
                    }
                    txtBuffer.append("\n" + OMC.getNombre() + " dice : " + OMC.getMensaje()); 
                }
            } catch(ClassNotFoundException cnfe) { 
                System.err.println(cnfe.getMessage()); 
            } catch(IOException ioe) {
                System.err.println(ioe.getMessage());
            } finally {
                cerrarCliente();
            }
        }

        /**
        * Metodo que cierra socket y flujos del cliente.
        */
        private void cerrarCliente() {
            try {
                flujoEntrada.close();
                sCli.close();
            } catch(IOException ioe) {
                System.err.println(ioe.getMessage());
            }
        }
  
    }
    /* *************************************************************** */
    /*                                                                 */
    /* *************************************************************** */

}
