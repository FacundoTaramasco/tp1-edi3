
// Clase lista enlazada
public class LEnlazada {

    private ElementoLista inicio = null;
    private ElementoLista fin    = null;

    public LEnlazada() {
        this(null);
    }

    /*
    Constructor
    */
    public LEnlazada(ElementoLista elemento) {
        setInicio(elemento);
    }

    public ElementoLista getInicio() {
        return this.inicio;
    }

    public ElementoLista getFin() {
        return this.fin;
    }

    private void setInicio(ElementoLista inicio) {
        this.inicio = inicio;
    }

    private void setFin(ElementoLista fin) {
        this.fin = fin;
    }

    // agrego elemento a la lista
    public void agregarElemento(ElementoLista elemento) {
        if (getInicio() == null) {
            setInicio(elemento);
            setFin(elemento);
        } else {
            getFin().setSiguiente(elemento);
            setFin(elemento);
        }
    }

    public void recorrerLista() {
        ElementoLista primero = getInicio();
        while (primero != null) {
            System.out.println(primero);
            primero = primero.getSiguiente();
        }
    }

}
