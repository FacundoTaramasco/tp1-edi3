
#include <stdio.h>

int FechaCorrecta(int dia, int mes, int anio);

int main(int argc, char **argv){
    int dia = 0;
    int mes = 0
    int anio = 0;
    char *meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio",
                    "Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
    printf("Ingrese fecha (formato = dd mm aaaa ) : ");
    scanf("%d %d %d", &dia, &mes, &anio);
    if (FechaCorrecta(dia, mes, anio) ){
        printf("\nFecha correcta : %d/%d/%d\n", dia, mes, anio);
        printf("%d de %s de %d", dia, meses[mes-1], anio);
    }
    else
        printf("Fecha incorrecta");
    return 0;

}
// funcion que valida
int FechaCorrecta(int dia, int mes, int anio){
    int diameses[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int diamesesb[12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    if ( (anio >= 0 && anio <= 9999 ) && (mes >=1  && mes <= 12 ) ) { //año y mes correcto
        if ( (anio % 4 == 0 && anio % 100 != 0) || anio % 400 == 0) { //año biciesto
            if (dia >= 1 && dia <= diamesesb[mes-1]) return 1; //dia correcto, fecha correcta
            else return 0; //dia incorrecto, fecha incorrecta
        }
        else //si no es año biciesto
            if (dia >= 1 && dia <= diameses[mes-1]) return 1; //dia correcto
            else return 0; //dia incorrecto, fecha incorrecta
    }
    else return 0;  //año o mes incorrecto
}
